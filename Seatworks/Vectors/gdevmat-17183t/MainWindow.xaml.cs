﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        const float BOTTOM_SCREEN = -20f;
        const float UPPER_SCREEN = 20f;
        const float RIGHT_SCREEN = 40f;
        const float LEFT_SCREEN = -40f;
        // Called every frame(Update)
        public Vector3 mousePosition = new Vector3();
        private Cube myCube = new Cube()
        {
            Position = new Vector3(-20, 0, 0),
            Mass = 0.5f
        };
        private Circle myCircle = new Circle()
        {
            Position = new Vector3(-20, 0, 0),
            Velocity = new Vector3(),
            Mass = 20f
            
        };
        private Vector3 wind = new Vector3(0.1f,0,0);
        private Vector3 verticalForce = new Vector3(0, 0.7f, 0);
        private Vector3 gravity = new Vector3 (0, -0.1f, 0);
        private Vector3 horizontalForce = new Vector3(0.2f, 0, 0);
        private Vector3 helium = new Vector3(0, 0.1f, 0);
        //public const float SCREEN_LIMIT = 10;
        
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 5";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer

            // Clear previous frame and display new one
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            // Move 40 pixels back
            gl.Translate(0.0f, 0.0f, -60.0f);
            
            gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, wind.x +" , " );

            myCube.Render(gl);
            myCircle.Render(gl);
           
            //if(Keyboard.IsKeyDown(Key.Space))
            //{
            //    myCube.ApplyForce(verticalForce);
            //    
            //}


            //myCube.ApplyForce(wind);

            //if (myCube.Position.x>=50)
            //{
            //    myCube.ApplyForce(wind-=wind);    
            //  
            //    myCube.Position.x = -50;
            //   myCube.Position.y = 0;
            //}

            myCube.ApplyForce(wind);
            myCube.ApplyForce(gravity);
            myCircle.ApplyForce(helium);
            myCircle.ApplyForce(wind);
      
            if (myCube.Position.y<=BOTTOM_SCREEN)
            {
               
                myCube.Velocity.y *= -1f;
            }
            if (myCube.Position.x >=RIGHT_SCREEN)
            {
            
                myCube.Velocity.x *= -1f;    
            }
           

            if (myCircle.Position.x >= RIGHT_SCREEN)
            {
                myCircle.Velocity.x *= -1f;
            }
            if (myCircle.Position.y >= UPPER_SCREEN)
            {
                myCircle.Velocity.y *= -1f;
            }
            
            
        }

        #region INITIALIZATION
        public MainWindow()
        {
           
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
         
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();
        }
    }

}
