﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;

namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int NORTH = 0;
        private const int SOUTH = 1;
        private const int WEST = 2;
        private const int EAST = 3;
        private const int NORTH_WEST = 4;
        private const int NORTH_EAST = 5;
        private const int SOUTH_WEST = 6;
        private const int SOUTH_EAST = 7;
        private const int CENTER = 8;

        private const int RED = 0;
        private const int GREEN = 1;
        private const int BLUE = 2;

    
        private Cube myFirstCube = new Cube(0,0);
       
        // Called every frame(Update)
       
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 2";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer

            // Clear previous frame and display new one
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            // Move 40 pixels back
            gl.Translate(0.0f, 0.0f, -40.0f);

            myFirstCube.Render(gl,0.5f);
            

            double outcome = RandomNumberGenerator.GenerateDouble(0, 1);
            double colorOutcome = RandomNumberGenerator.GenerateDouble(0, 8);
            
            //Position
            //switch (outcome)
            //{
            //    case NORTH:
            //        myFirstCube.posY++;
            //        break;
            //    case SOUTH:
            //        myFirstCube.posY--;
            //        break;
            //    case WEST:
            //        myFirstCube.posX--;
            //        break;
            //    case EAST:
            //        myFirstCube.posX++;
            //        break;
            //    case NORTH_WEST:
            //        myFirstCube.posY++;
            //        myFirstCube.posX--;
            //        break;
            //    case NORTH_EAST:
            //        myFirstCube.posY++;
            //        myFirstCube.posX++;
            //        break;
            //    case SOUTH_WEST:
            //        myFirstCube.posY--;
            //        myFirstCube.posX--;
            //        break;
            //    case SOUTH_EAST:
            //        myFirstCube.posY--;
            //        myFirstCube.posX++;
            //        break;
            //    case CENTER:
            //        break;
            //}
            
            if (outcome<=0.2)
            {
                myFirstCube.posY++;
            }
            else if (outcome>0.2 && outcome<=0.4)
            {
                myFirstCube.posY--;
            }
            else if (outcome>0.4 && outcome<=0.6)
            {
                myFirstCube.posX--;
            }
            else if (outcome>0.6)
            {
                myFirstCube.posX++;
            }
            
            myFirstCube.color = new Models.Color(RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1), RandomNumberGenerator.GenerateDouble(0, 1));




        }

        #region INITIALIZATION
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion
    }
}
