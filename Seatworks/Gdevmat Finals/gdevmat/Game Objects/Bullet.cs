﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gdevmat.Models;
using gdevmat.Utilities;
using SharpGL;
using System.Windows.Input;
namespace gdevmat.Game_Objects
{
    public class Bullet:Movable
    {
        public Vector3 anchorX1;
        public Vector3 anchorX2;
        public Vector3 anchorX3;
        public Vector3 anchorX4;
        public Vector3 Scale = new Vector3(0.15f, 0.5f, 0.15f);
        public float MoveSpeed = 5;
        public bool hasCollided;
        public Bullet()
        {

        }
        public Bullet(float x, float y, float z)
        {
            this.Position.x = x;
            this.Position.y = y;
            this.Position.z = z;
       
        }
   
        public void checkCollision(Asteroid asteroidToCheck)
        {
            if (this.Position.x >= asteroidToCheck.anchorX1.x && this.Position.x <= asteroidToCheck.anchorX2.x && this.Position.y >= asteroidToCheck.anchorX3.y && this.Position.y <= asteroidToCheck.anchorX4.y)
            {
                hasCollided = true;
                asteroidToCheck.Shrink();
                asteroidToCheck.Position.y += 5;
            }
            
        }
       
        public void updateHitbox()
        {
            hasCollided = false;
            anchorX1 = new Vector3(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            anchorX2 = new Vector3(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            anchorX3 = new Vector3(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            anchorX4 = new Vector3(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);

        }
        public override void Render(OpenGL gl)
        {
            base.Render(gl);
            updateHitbox();
     
            #region
            gl.Begin(OpenGL.GL_TRIANGLE_STRIP);
            //Front face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            //Right face
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z - this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z - this.Scale.z);
            //Back face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z - this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z - this.Scale.z);
            //Left face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            gl.End();
            gl.Begin(OpenGL.GL_TRIANGLE_STRIP);
            //Top face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y + this.Scale.y, this.Position.z - this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y + this.Scale.y, this.Position.z - this.Scale.z);
            gl.End();
            gl.Begin(OpenGL.GL_TRIANGLE_STRIP);
            //Bottom face
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z + this.Scale.z);
            gl.Vertex(this.Position.x - this.Scale.x, this.Position.y - this.Scale.y, this.Position.z - this.Scale.z);
            gl.Vertex(this.Position.x + this.Scale.x, this.Position.y - this.Scale.y, this.Position.z - this.Scale.z);
            gl.End();
            #endregion
        }
        public void Movement()
        {

           
                this.Position.y += MoveSpeed;
           

        }   
    }
    }
