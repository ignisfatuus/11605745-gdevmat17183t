﻿using gdevmat.Game_Objects;
using gdevmat.Models;
using gdevmat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace gdevmat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
                                        
        
    public partial class MainWindow : Window
    {
        #region Initialization
        public MainWindow()
        {
            InitializeComponent();
     
       
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);
            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        #endregion


        private List<Bullet> Bullets = new List<Bullet>();
        private List<Asteroid> Asteroids = new List<Asteroid>();
        private Spaceship SpaceS = new Spaceship(0, -25, 0);
        public Bullet Bullet = new Bullet();
        private Vector3 mousePosition = new Vector3();
        private Asteroid Asteroidz = new Asteroid(0, 25, 0,8);
        private List<Vector3> randomForces = new List<Vector3>();

        int frameCount;
        int Score;
        void LimitMovement(Spaceship SpaceS, float xLimit, float yLimit)
        {
            if (SpaceS.hasCollided) return;
            if (SpaceS.Position.x <= -xLimit) SpaceS.Position.x = -xLimit;
            if (SpaceS.Position.x >= xLimit) SpaceS.Position.x = xLimit;
            if (SpaceS.Position.y <= -yLimit) SpaceS.Position.y = -yLimit;
            if (SpaceS.Position.y >= yLimit) SpaceS.Position.y = yLimit;
        }
         void Shoot( OpenGL gl,Spaceship Player)
        {
            
            if (Keyboard.IsKeyDown(Key.Space))
            {
                Bullets.Add(new Bullet(Player.Position.x, Player.Position.y+1, Player.Position.z));   
               

            }
        
        }

        void SpawnAsteroids(int frameCount, int Number)
        {
            if (frameCount % Number == 0)
            {
                Asteroids.Add(new Asteroid((float)RandomNumberGenerator.GenerateFloat(-45, 45), 25, 0, 5));
            }
            if (frameCount%500==0)
            {
                Number -= 5; // pambilis spawn
            }

        }
       
        void ShowGameOver(Spaceship SpaceS, OpenGL gl)
        {
            if(SpaceS.hasCollided)
            {
                gl.DrawText(2555, 2555, 1, 1, 1, "Arial", 25, "GAME OVER");
            }
        }
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            // Clear The Screen And The Depth Buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            // Move Left And Into The Screen
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            frameCount += 1;
       

            SpawnAsteroids(frameCount,15);
            SpaceS.MoveSpeed = 5;    
            SpaceS.Render(gl);
            LimitMovement(SpaceS, 65, 35);
            SpaceS.Movement();
            Shoot(gl, SpaceS);
            ShowGameOver(SpaceS, gl);
            foreach (var c in Bullets)
            {
                c.Render(gl);
                c.Movement();

            }

            foreach (var c in Asteroids)
            {
                c.Render(gl);
                c.Movement();

                Score = c.Destroy(Score);
            }

            foreach (var z in Asteroids)
            {
                SpaceS.checkCollision(z);
                foreach (var c in Bullets)
                {
                    c.checkCollision(z);

                }
            }
            gl.DrawText(0, 25, 1, 1, 1, "Arial", 25, Score.ToString());
        }

        #region Mouse Func
        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
            mousePosition.Normalize();

        }
        #endregion
    }
}
