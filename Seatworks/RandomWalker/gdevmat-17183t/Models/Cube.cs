﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Cube
    {
        public Color color = new Color();
        public float posX, posY;
        public float size;
        
        // Constructor
        public Cube(float x, float y)
        {
            this.posX = x;
            this.posY = y;
        }

        public void Render(OpenGL gl,float cubeSize)
        {
            gl.Color(color.red, color.green, color.blue);
            gl.Begin(OpenGL.GL_QUADS);
            //Front face
            gl.Vertex(this.posX - cubeSize, this.posY + cubeSize);
            gl.Vertex(this.posX + cubeSize, this.posY + cubeSize);
            gl.Vertex(this.posX + cubeSize, this.posY - cubeSize);
            gl.Vertex(this.posX - cubeSize, this.posY - cubeSize);

        

            gl.End();
        }
    }
}
